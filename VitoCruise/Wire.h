/*
 * Wire.h
 *
 * Created: 8/04/2018 1:03:32
 *  Author: robbe_000
 */ 


#ifndef WIRE_H_
	#define WIRE_H_
	
	#include <avr/io.h>
	#include <util/delay.h>
	
	#include "twiLib.h"

	#define WIRE_BUFFER_LENGTH 32

	void Wire_init();
	void Wire_beginTransmission(uint8_t addr);
	uint8_t Wire_endTransmission(uint8_t sendStop);
	uint8_t Wire_endTransmissionWithStop();
	void Wire_write(uint8_t data);
	void Wire_writeBuffer(uint8_t *data, uint8_t length);
	
#endif /* WIRE_H_ */