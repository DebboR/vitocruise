/*
 * Security.h
 *
 * Created: 29/05/2018 23:27:48
 *  Author: robbe_000
 */ 


#ifndef SECURITY_H_
#define SECURITY_H_

#include <avr/io.h>

void Security_init();
uint8_t Security_isBraking();
void Security_check();


#endif /* SECURITY_H_ */