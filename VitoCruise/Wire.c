/*
 * Wire.c
 *
 * Created: 8/04/2018 1:03:23
 *  Author: robbe_000
 */ 

 #include "Wire.h"

 // Global Wire Variables
 uint8_t rxBuffer[WIRE_BUFFER_LENGTH];
 uint8_t rxBufferIndex = 0;
 uint8_t rxBufferLength = 0;

 uint8_t txAddress = 0;
 uint8_t txBuffer[WIRE_BUFFER_LENGTH];
 uint8_t txBufferIndex = 0;
 uint8_t txBufferLength = 0;

 uint8_t transmitting = 0;

 // "Methods"
 void Wire_init(){
	rxBufferIndex = 0;
	rxBufferLength = 0;

	txBufferIndex = 0;
	txBufferLength = 0;

	twi_init();
 }

 void Wire_beginTransmission(uint8_t addr){
	// Indicate that we are transmitting
	transmitting = 1;

	// Set address of targeted slave
	txAddress = addr;

	// Reset buffer iterator variables
	txBufferIndex = 0;
	txBufferLength = 0;
 }

 uint8_t Wire_endTransmission(uint8_t sendStop){
	// Transmit buffer (blocking)
	uint8_t returnValue = twi_writeTo(txAddress, txBuffer, txBufferLength, 1, sendStop);

	// Reset buffer iterator vars
	txBufferIndex = 0;
	txBufferLength = 0;

	// Indicate that we are done transmitting
	transmitting = 0;
	
	return returnValue;
 }

 uint8_t Wire_endTransmissionWithStop(){
	return Wire_endTransmission(1);
 }

 void Wire_write(uint8_t data){
	 if(transmitting){
		 // In master transmitter mode
		 // Don't bother if buffer is full
		 if(txBufferLength >= WIRE_BUFFER_LENGTH)
			 return;

		 // Put byte in tx buffer
		 txBuffer[txBufferIndex] = data;
		 txBufferIndex++;

		 // Update amount in buffer
		 txBufferLength = txBufferIndex;

	 } else {
		 // In slave send mode
		 // Reply to master
		 twi_transmit(&data, 1);
	 }
 }

 void Wire_writeBuffer(uint8_t *data, uint8_t length){
	if(transmitting){
		// In master transmitter mode
		for(uint8_t i = 0; i < length; ++i)
			Wire_write(data[i]);
	} else {
		// In slave send mode
		// Reply to master
		twi_transmit(data, length);
	}
 }