/*
 * Watchdog.h
 *
 * Created: 26/05/2018 23:38:31
 *  Author: robbe_000
 */ 


#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include <avr/io.h>
#include <avr/wdt.h>

#define WATCHDOG_MODULE_CRITICAL_LOOP 0
#define WATCHDOG_MODULE_MAIN_LOOP 1
#define WATCHDOG_MODULE_SPEED_LOOP 2
#define WATCHDOG_MODULE_SECURITY 3
#define WATCHDOG_MASK ((1 << WATCHDOG_MODULE_MAIN_LOOP) | (1 << WATCHDOG_MODULE_CRITICAL_LOOP) | (1 << WATCHDOG_MODULE_SPEED_LOOP) | (1 << WATCHDOG_MODULE_SECURITY))

void Watchdog_init();
void Watchdog_kick(uint8_t module);

#endif /* WATCHDOG_H_ */