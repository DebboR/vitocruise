/*
 * CruiseControl.h
 *
 * Created: 6/05/2018 3:52:37
 *  Author: robbe_000
 */ 


#ifndef CRUISECONTROL_H_
#define CRUISECONTROL_H_

#include <avr/io.h>

// User parameters
#define MAX_SPEED 140
#define MIN_SPEED 20
#define FAST_INCREMENT_BASE 5
#define MAX_ACCELERATION_KMHS 2
#define KP 30.0			// Proportional gain
#define KPV 6.0			// Proportional speed gain
#define TI 5.0			// Integrator time constant
#define KS 1.0			// Anti-windup gain

// Calculated parameters
#define DT (1/30.5)
#define RAMP_INCREMENT (MAX_ACCELERATION_KMHS * DT)

void CC_init();
void CC_switchOn();
void CC_switchOff();
void CC_incrementSetSpeed();
void CC_decrementSetSpeed();
void CC_incrementSetSpeedFast();
void CC_decrementSetSpeedFast();
void CC_setSetSpeed(uint8_t speed);
void CC_switchReset();
uint8_t CC_isOn();
uint8_t CC_getSetSpeed();

void CC_calculate();
float CC_getSetpoint();
float CC_getError();
float CC_getI();
float CC_getPV();
uint16_t CC_getIntegerResult();

#endif /* CRUISECONTROL_H_ */