/*
 * Watchdog.c
 *
 * Created: 26/05/2018 23:38:19
 *  Author: robbe_000
 */ 

 #include "Watchdog.h"

 uint8_t WatchdogStatus = 0;

 void Watchdog_init(){
	// Enable WDT with a time-out of 1s
	wdt_enable(WDTO_1S);
 }

 void Watchdog_kick(uint8_t module){

	WatchdogStatus |= (1 << module);

	// Make sure all modules have kicked before actually resetting WDT
	if(WatchdogStatus == WATCHDOG_MASK){
		// Reset watchdog timer
		wdt_reset();

		// Reset mask
		WatchdogStatus = 0;
	}	
 }