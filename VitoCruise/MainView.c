/*
 * MainView.c
 *
 * Created: 6/05/2018 2:43:07
 *  Author: robbe_000
 */ 

#include "MainView.h"

void MainView_render(){
	// Draw pedal position
	GFX_drawVProgressBar(1, 1, 10, HEIGHT - 2, 100, Throttle_getPositionScaled(0, 100), WHITE);

	// Draw set throttle
	GFX_drawVProgressBar(WIDTH - 12, 1, 10, HEIGHT - 2, 100, Throttle_getSetPositionScaled(0, 100), WHITE);

	// Draw current speed
	unsigned char tmp[5];
	uint8_t speed = Speed_getIntegerSpeed();
	sprintf(tmp, "%d", speed);
	const uint8_t SCALE = 3;

	if(speed < 10) {
		GFX_drawString(((WIDTH - (SCALE * CHAR_WIDTH)) / 2), 5, tmp, 1, WHITE, BLACK, SCALE);
	} else if (speed < 100) {
		GFX_drawString(((WIDTH - 2 * (SCALE * CHAR_WIDTH)) / 2), 5, tmp, 2, WHITE, BLACK, SCALE);
	} else {
		GFX_drawString(((WIDTH - 3 * (SCALE * CHAR_WIDTH)) / 2), 5, tmp, 3, WHITE, BLACK, SCALE);
	}

	// Draw cruise control mode
	GFX_drawString(21, HEIGHT - 30, "Cruise control:", 15, WHITE, BLACK, 1);
	if(CC_isOn()){
		uint8_t setSpeed = CC_getSetSpeed();
		sprintf(tmp, "%d", setSpeed);
		if(setSpeed < 10) {
			GFX_drawString(((WIDTH - (2 * CHAR_WIDTH)) / 2), HEIGHT - 18, tmp, 1, WHITE, BLACK, 2);
		} else if (setSpeed < 100) {
			GFX_drawString(((WIDTH - 2 * (2 * CHAR_WIDTH)) / 2), HEIGHT - 18, tmp, 2, WHITE, BLACK, 2);
		} else {
			GFX_drawString(((WIDTH - 3 * (2 * CHAR_WIDTH)) / 2), HEIGHT - 18, tmp, 3, WHITE, BLACK, 2);
		}
	} else {
		GFX_drawString(50, HEIGHT - 18, "OFF", 3, WHITE, BLACK, 2);
	}

}