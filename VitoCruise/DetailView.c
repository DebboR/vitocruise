/*
 * DetailView.c
 *
 * Created: 30/05/2018 0:50:33
 *  Author: robbe_000
 */ 

 #include "DetailView.h"

 void DetailView_render(){
	// Draw pedal position
	GFX_drawVProgressBar(1, 1, 10, HEIGHT - 2, 100, Throttle_getPositionScaled(0, 100), WHITE);

	// Draw set throttle
	GFX_drawVProgressBar(WIDTH - 12, 1, 10, HEIGHT - 2, 100, Throttle_getSetPositionScaled(0, 100), WHITE);

	// Draw current speed
	unsigned char tmp[15];
	memset(tmp, 0, 15);
	uint8_t speed = Speed_getIntegerSpeed();
	sprintf(tmp, "Spd: %d", speed);
	GFX_drawString(15, 4, tmp, 8, WHITE, BLACK, 1);
	 
	// Draw cruise control mode
	GFX_drawString(70, 4, "CC: ", 4, WHITE, BLACK, 1);
	if(CC_isOn()){
		uint8_t setSpeed = CC_getSetSpeed();
		memset(tmp, 0, 15);
		sprintf(tmp, "%d", setSpeed);
		GFX_drawString(90, 4, tmp, 3, WHITE, BLACK, 1);
	} else {
		GFX_drawString(90, 4, "OFF", 3, WHITE, BLACK, 1);
	}

	// Draw braking
	if(Security_isBraking())
		GFX_drawString(15, 14, "Brk: ON", 7, WHITE, BLACK, 1);
	else
		GFX_drawString(15, 14, "Brk: OFF", 8, WHITE, BLACK, 1);

	// Draw setpoint
	memset(tmp, 0, 15);
	sprintf(tmp, "Setp: %.1f", CC_getSetpoint());
	GFX_drawString(15, 24, tmp, 15, WHITE, BLACK, 1);

	// Draw error
	memset(tmp, 0, 15);
	sprintf(tmp, "Err: %.1f", CC_getError());
	GFX_drawString(15, 34, tmp, 15, WHITE, BLACK, 1);

	// Draw I
	memset(tmp, 0, 15);
	sprintf(tmp, "I: %.1f", CC_getI());
	GFX_drawString(15, 44, tmp, 15, WHITE, BLACK, 1);
	
	// Draw PV
	memset(tmp, 0, 15);
	sprintf(tmp, "PV: %.1f", CC_getPV());
	GFX_drawString(15, 54, tmp, 15, WHITE, BLACK, 1);

 }