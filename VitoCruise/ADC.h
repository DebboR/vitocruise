/*
 * ADC.h
 *
 * Created: 27/04/2018 23:57:18
 *  Author: robbe_000
 */ 


#ifndef ADC_H_
#define ADC_H_

#include <avr/interrupt.h>

void ADC_init();

uint16_t ADC_getADC0();
uint16_t ADC_getADC1();

#endif /* ADC_H_ */