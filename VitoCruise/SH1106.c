/*
 * SH1106.c
 *
 * Created: 11/25/2017 3:33:33 AM
 *  Author: robbe_000
 */ 

 // Based on: https://github.com/wonho-maker/Adafruit_SH1106/blob/master/Adafruit_SH1106.cpp

 #include <avr/io.h>

 #include "SH1106.h"

 // Memory buffer:
 #define BUFFER_SIZE (SH1106_LCDHEIGHT * SH1106_LCDWIDTH / 8)
 static uint8_t buffer[BUFFER_SIZE];

 #define SH1106_SWAP(a, b) {int_16_t t = a; a = b; b = t;}

 void SH1106_init(){
	Wire_init();
	 
	// Init sequence for 128x64 OLED module
	SH1106_command(SH1106_DISPLAYOFF);                    // 0xAE
	SH1106_command(SH1106_SETDISPLAYCLOCKDIV);            // 0xD5
	SH1106_command(0x80);                                 // Suggested ratio: 0x80
	SH1106_command(SH1106_SETMULTIPLEX);                  // 0xA8
	SH1106_command(0x3F);
	SH1106_command(SH1106_SETDISPLAYOFFSET);              // 0xD3
	SH1106_command(0x00);                                 // no offset
	  
	SH1106_command(SH1106_SETSTARTLINE | 0x0);            // line #0 0x40
	SH1106_command(SH1106_CHARGEPUMP);                    // 0x8D
	SH1106_command(0x14);
	SH1106_command(SH1106_MEMORYMODE);                    // 0x20
	SH1106_command(0x00);                                 // 0x0: Act like ks0108
	SH1106_command(SH1106_SEGREMAP | 0x1);
	SH1106_command(SH1106_COMSCANDEC);
	SH1106_command(SH1106_SETCOMPINS);                    // 0xDA
	SH1106_command(0x12);
	SH1106_command(SH1106_SETCONTRAST);                   // 0x81
	SH1106_command(0xCF);
	SH1106_command(SH1106_SETPRECHARGE);                  // 0xd9
	SH1106_command(0xF1);
	SH1106_command(SH1106_SETVCOMDETECT);                 // 0xDB
	SH1106_command(0x40);
	SH1106_command(SH1106_DISPLAYALLON_RESUME);           // 0xA4
	SH1106_command(SH1106_NORMALDISPLAY);                 // 0xA6
	
	SH1106_command(SH1106_DISPLAYON);					  //Turn on OLED-panel
 }
 
 void SH1106_command(uint8_t command){
	Wire_beginTransmission(SH1106_TWI_ADDRESS);
	Wire_write(0x00); //Control
	Wire_write(command);
	Wire_endTransmissionWithStop();
 }

 void SH1106_data(uint8_t data){
	Wire_beginTransmission(SH1106_TWI_ADDRESS);
	Wire_write(0x40); //Control
	Wire_write(data);
	Wire_endTransmissionWithStop();
 }

 void SH1106_display(){
	SH1106_command(SH1106_SETLOWCOLUMN | 0x0);		// Low column = 0
	SH1106_command(SH1106_SETHIGHCOLUMN | 0x0);		// High column = 0
	SH1106_command(SH1106_SETSTARTLINE | 0x0);		// Start line = 0

	uint8_t height = 64;
	uint8_t width = 132;
	uint8_t m_row = 0;
	uint8_t m_col = 2;
	
	height >>= 3;
	width >>= 3;
	
	uint16_t bufferPointer = 0;
		
	for (uint8_t row = 0; row < height; row++) {
			
		// Send a bunch of data in one transmission
		SH1106_command(0xB0 + row + m_row);							// Set page address
		SH1106_command(SH1106_SETLOWCOLUMN  | (m_col & 0xf));			// Set lower column address
		SH1106_command(SH1106_SETHIGHCOLUMN | (m_col >> 4));		// Set higher column address
			
		for(uint8_t j = 0; j < 8; j++){
			Wire_beginTransmission(SH1106_TWI_ADDRESS);
			Wire_write(0x40);
			for (uint8_t column = 0; column < width; column++, bufferPointer++)
				Wire_write(buffer[bufferPointer]);
			Wire_endTransmissionWithStop();
		}
	}
 }

void SH1106_drawPixel(int16_t x, int16_t y, uint8_t color){
	// Check if coordinate is on screen.
	if ((x < 0) || (x >= SH1106_LCDWIDTH) || (y < 0) || (y >= SH1106_LCDHEIGHT))
		return;

	switch(color){
		case WHITE: buffer[x+ (y/8)*SH1106_LCDWIDTH] |=  (1 << (y&7)); break;
		case BLACK:   buffer[x+ (y/8)*SH1106_LCDWIDTH] &= ~(1 << (y&7)); break;
		case INVERSE: buffer[x+ (y/8)*SH1106_LCDWIDTH] ^=  (1 << (y&7)); break;
		default: break;
	}
}

void SH1106_invertDisplay(uint8_t invert){
	SH1106_command(invert ? SH1106_INVERTDISPLAY : SH1106_NORMALDISPLAY);
}

void SH1106_clearBuffer(){
	memset(buffer, 0, BUFFER_SIZE);
}






 