/*
 * MainView.h
 *
 * Created: 6/05/2018 2:43:18
 *  Author: robbe_000
 */ 


#ifndef MAINVIEW_H_
#define MAINVIEW_H_

#include <stdio.h>
#include <math.h>

#include "GFX.h"
#include "Throttle.h"
#include "Speed.h"
#include "CruiseControl.h"

void MainView_render();

#endif /* MAINVIEW_H_ */