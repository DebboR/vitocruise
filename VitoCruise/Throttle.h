/*
 * Throttle.h
 *
 * Created: 28/04/2018 0:49:25
 *  Author: robbe_000
 */ 


#ifndef THROTTLE_H_
#define THROTTLE_H_

#include <avr/io.h>
#include <math.h>
#include <stdlib.h>

#include "ADC.h"
#include "DAC.h"

#define MAX_CHANNEL_DIFFERENCE 20
#define THROTTLE_MIN_POS 0
#define THROTTLE_MAX_POS 930

#define THROTTLE_SET_MIN_POS 0
#define THROTTLE_SET_MAX_POS 1023

uint16_t Throttle_getPosition();
uint16_t Throttle_getPositionScaled(uint16_t minVal, uint16_t maxVal);
void Throttle_setPosition(uint16_t position);
uint16_t Throttle_getSetPosition();
uint16_t Throttle_getSetPositionScaled(uint16_t minVal, uint16_t maxVal);
uint16_t Throttle_limit(float input);

#endif /* THROTTLE_H_ */