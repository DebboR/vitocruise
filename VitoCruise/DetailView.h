/*
 * DetailView.h
 *
 * Created: 30/05/2018 0:50:05
 *  Author: robbe_000
 */ 


#ifndef DETAILVIEW_H_
#define DETAILVIEW_H_

#include <stdio.h>
#include <math.h>

#include "GFX.h"
#include "Throttle.h"
#include "Speed.h"
#include "CruiseControl.h"
#include "Security.h"

void DetailView_render();

#endif /* DETAILVIEW_H_ */