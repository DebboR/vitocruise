/*
 * Speed.h
 *
 * Created: 6/05/2018 3:01:30
 *  Author: robbe_000
 */ 


#ifndef SPEED_H_
#define SPEED_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>

// 415 Hz = 50km/h
#define PULSES_PER_KMH 6.8

void Speed_init();
float Speed_getSpeed();
uint8_t Speed_getIntegerSpeed();

#endif /* SPEED_H_ */
