#ifndef TWI_LIB_H
	#define TWI_LIB_H

	#include <avr/io.h>
	#include <avr/interrupt.h>
	#include <compat/twi.h>

	#define TWI_FREQ 100000L
	#define TWI_BUFFER_LENGTH 32
	#define TWI_READY 0
	#define TWI_MRX   1
	#define TWI_MTX   2
	#define TWI_SRX   3
	#define TWI_STX   4

	void twi_init();
	uint8_t twi_writeTo(uint8_t address, uint8_t* data, uint8_t length, uint8_t wait, uint8_t sendStop);
	uint8_t twi_transmit(uint8_t* data, uint8_t length);
	void twi_reply(uint8_t);
	void twi_stop();
	void twi_releaseBus();
	void twi_reset();
#endif