/*
 * Controls.h
 *
 * Created: 26/05/2018 13:25:09
 *  Author: robbe_000
 */ 


#ifndef CONTROLS_H_
#define CONTROLS_H_

#include <avr/io.h>

// Define if the control switch should also be checked before registering switches
#define CHECK_CONTROL 0

// Define the number of handling cycles the controls must go through before engaging fast modus
#define FAST_MODE_LAG 5
// Define the number of handling cycles the controls go through in one call period in fast modus
#define FAST_MODE_DELAY 3

void Controls_init();
void Controls_handle();

void Controls_onUp();
void Controls_onDown();
void Controls_onFront();
void Controls_onBack();

void Controls_onFastUp();
void Controls_onFastDown();
void Controls_onFastFront();
void Controls_onFastBack();

#endif /* CONTROLS_H_ */