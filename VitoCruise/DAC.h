/*
 * DAC.h
 *
 * Created: 29/04/2018 18:04:57
 *  Author: robbe_000
 */ 


#ifndef DAC_H_
#define DAC_H_

#include <avr/interrupt.h>
#include <util/delay.h>

void DAC_init();
void DAC_sendCommand(uint16_t command);
void DAC_setDAC0(uint16_t value);			// Beware: 12 bits!
void DAC_setDAC1(uint16_t value);			// Beware: 12 bits!

#endif /* DAC_H_ */