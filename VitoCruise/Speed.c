/*
 * Speed.c
 *
 * Created: 6/05/2018 3:01:06
 *  Author: robbe_000
 */ 

 #include "Speed.h"

 #include "Throttle.h"
 #include "Watchdog.h"

 uint16_t SpeedCount = 0;
 float Speed = 0;

void Speed_init(){
	// Enable external interrupt 7 on any change
	EICRB |= (1 << ISC70);
	EIMSK |= (1 << INT7);

	// Timer 1
	// Mode: CTC
	// Clock divider: 64
	// Top value: 50000 (2.5 / sec)
	// Enable compare interrupt
	TCCR1B = (1 << CS11) | (1 << CS10) | (1 << WGM12);
	OCR1A = 50000;
	TIMSK1 = (1 << OCIE1A);
}

float Speed_getSpeed(){
	return Speed;
	//return Throttle_getPositionScaled(0, 250);
}

uint8_t Speed_getIntegerSpeed(){
	return (uint8_t) round(Speed_getSpeed());
}

// Executed on level change
ISR(INT7_vect){
	SpeedCount++;
}

// Executed by timer 1 CTC (5 / sec)
ISR(TIMER1_COMPA_vect){
	// Calculate effective speed
	Speed = ((float) SpeedCount) / ((float) PULSES_PER_KMH);

	// Reset counter
	SpeedCount = 0;

	// Kick watchdog
	Watchdog_kick(WATCHDOG_MODULE_SPEED_LOOP);
}
