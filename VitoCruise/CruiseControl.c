/*
 * CruiseControl.c
 *
 * Created: 6/05/2018 3:52:26
 *  Author: robbe_000
 */ 

#include "CruiseControl.h"

#include "Speed.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

uint8_t CCOn = 0;
uint8_t CCSetSpeed = 0;
uint8_t CCPreviousSpeed = 0;

float CCError = 0;
float CCSetpoint = 0;

float CCI = 0;
float CCPV = 0;
float CCSAT = 0;
float CCResult = 0;

void CC_init(){
	
}

void CC_switchOn(){
	// Check if speed is OK to turn on
	if(Speed_getSpeed() >= MIN_SPEED){
		CCOn = 1;
		CC_setSetSpeed(Speed_getIntegerSpeed());
	}
}

void CC_switchOff(){
	CCOn = 0;
	CC_setSetSpeed(0);
	CCSetpoint = 0;
}

void CC_incrementSetSpeed(){
	CC_setSetSpeed(CC_getSetSpeed() + 1);
}

void CC_decrementSetSpeed(){
	CC_setSetSpeed(CC_getSetSpeed() - 1);
}

void CC_incrementSetSpeedFast(){
	CC_setSetSpeed((FAST_INCREMENT_BASE * ((uint8_t) floor(((float) CC_getSetSpeed()) / FAST_INCREMENT_BASE))) + FAST_INCREMENT_BASE);
}

void CC_decrementSetSpeedFast(){
	CC_setSetSpeed((FAST_INCREMENT_BASE * ((uint8_t) ceil(((float) CC_getSetSpeed()) / FAST_INCREMENT_BASE))) - FAST_INCREMENT_BASE);
}

void CC_setSetSpeed(uint8_t speed){
	// Only change set speed when CC is turned on
	if(CC_isOn()){
		// Keep set speed within bounds
		CCSetSpeed = MAX(MIN(MAX_SPEED, speed), MIN_SPEED);
		CCPreviousSpeed = CCSetSpeed;

		// Set setpoint to current speed to start ramp up / down
		CCSetpoint = Speed_getSpeed();
	}
}

void CC_switchReset(){
	if(CC_isOn()){
		// If Cruise Control is switched on, turn it off
		CC_switchOff();
	} else {
		// If Cruise Control is switched off, turn it on and reset to previous speed
		// Check if speed is OK to turn on
		if(Speed_getSpeed() >= MIN_SPEED){
			CCOn = 1;
			CC_setSetSpeed(CCPreviousSpeed);
		}
	}
}

uint8_t CC_isOn(){
	return CCOn;
}

uint8_t CC_getSetSpeed(){
	return CCSetSpeed;
}

void CC_calculate(){
	// Check if CC is on
	if(!CC_isOn()){
		CCError = 0;
		CCResult = 0;
		CCI = 0;
		CCPV = 0;
		return;
	}

	// Do ramp calculations
	if(fabs(CCSetpoint - CC_getSetSpeed()) > RAMP_INCREMENT)
		CCSetpoint += ((CCSetpoint < CC_getSetSpeed()) ? (RAMP_INCREMENT) : (-1*RAMP_INCREMENT));
	else
		CCSetpoint = CC_getSetSpeed();

	// Calculate error
	CCError = CCSetpoint - Speed_getSpeed();

	// Check saturation
	CCSAT = CCResult - Throttle_limit(CCResult);

	// Integrate
	CCI += (CCError - (KS * CCSAT)) * (DT / TI);
	
	// Speed gain
	CCPV = KPV * CCSetpoint;

	// Total
	CCResult = KP * (CCError + CCI) + CCPV;
}

float CC_getError(){
	return CCError;
}

float CC_getSetpoint(){
	return CCSetpoint;
}

float CC_getI(){
	return CCI;
}

float CC_getPV(){
	return CCPV;
}

uint16_t CC_getIntegerResult(){
	return Throttle_limit(CCResult);
}
