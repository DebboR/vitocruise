/*
* twi.c
*
* Created: 11/25/2017 3:18:46 AM
*  Author: robbe_000
*/

#ifndef  F_CPU
	#define F_CPU 8000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>

#include "twiLib.h"

uint8_t twi_state;
uint8_t twi_slarw;
uint8_t twi_sendStop;
uint8_t twi_inRepStart;
uint8_t twi_error;

uint8_t twi_masterBuffer[TWI_BUFFER_LENGTH];
uint8_t twi_masterBufferIndex;
uint8_t twi_masterBufferLength;

uint8_t twi_txBuffer[TWI_BUFFER_LENGTH];
uint8_t twi_txBufferIndex;
uint8_t twi_txBufferLength;

uint8_t twi_rxBuffer[TWI_BUFFER_LENGTH];
uint8_t twi_rxBufferIndex;

void twi_init(void){
	// Initialize state
	twi_state = TWI_READY;
	twi_sendStop = 1;			// default value
	twi_inRepStart = 0;

	// Set output
	DDRD = 0b00100000;

	// Initialize TWI prescaler and bit rate
	TWSR = 0b00000000;
	TWBR = ((F_CPU / TWI_FREQ) - 16) / 2;
	
	// Enable TWI module, ACKs, and TWI interrupt
	TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA);
}

uint8_t twi_writeTo(uint8_t address, uint8_t* data, uint8_t length, uint8_t wait, uint8_t sendStop){
	// Ensure data will fit into buffer
	if(TWI_BUFFER_LENGTH < length)
		return 1;

	// Wait until TWI is ready, become master transmitter
	uint16_t counter = 0;
	while(TWI_READY != twi_state){
		_delay_us(20);
		if(counter++ > 2000){
			twi_reset();
			return 5;	// Error timing out
		}
	}

	twi_state = TWI_MTX;
	twi_sendStop = sendStop;

	// Reset error state (0xFF.. no error occurred)
	twi_error = 0xFF;

	// Initialize buffer iteration vars
	twi_masterBufferIndex = 0;
	twi_masterBufferLength = length;
	
	// Copy data to TWI buffer
	for(uint8_t i = 0; i < length; i++)
		twi_masterBuffer[i] = data[i];
	
	// Build sla+w, slave device address + w bit
	twi_slarw = TW_WRITE;
	twi_slarw |= address << 1;
	
	// If we're in a repeated start, then we've already sent the START in the ISR. Don't do it again.
	if (twi_inRepStart) {
		twi_inRepStart = 0;
		do {
			TWDR = twi_slarw;
		} while(TWCR & (1 << TWWC));
		TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWEN) | (1 << TWIE);	// enable INTs, but not START
	} else {
		// Send START condition
		TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWEN) | (1 << TWIE) | (1 << TWSTA);	// enable INTs
	}

	// Wait for write operation to complete
	counter=0;
	while(wait && (TWI_MTX == twi_state)){
		_delay_us(10);
		if(counter++>2000){
			twi_reset();
			return 5;	// Error timing out
		}
	}
		
	// Return errors
	if (twi_error == 0xFF)
		return 0;	// Success
	else if (twi_error == TW_MT_SLA_NACK)
		return 2;	// Error: address send, NACK received
	else if (twi_error == TW_MT_DATA_NACK)
		return 3;	// Error: data send, NACK received
	else
		return 4;	// Other TWI error
}

uint8_t twi_transmit(uint8_t* data, uint8_t length){
	// Ensure data will fit into buffer
	if(TWI_BUFFER_LENGTH < (twi_txBufferLength + length))
		return 1;
	  
	// Ensure we are currently a slave transmitter
	if(TWI_STX != twi_state)
		return 2;
	  
	// Set length and copy data into tx buffer
	for(uint8_t i = 0; i < length; i++)
		twi_txBuffer[twi_txBufferLength + i] = data[i];
	twi_txBufferLength += length;
	  
	return 0;
}

void twi_stop(){
	// Send stop condition
	TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA) | (1 << TWINT) | (1 << TWSTO);

	// Wait for stop condition to be executed on bus
	// TWINT is not set after a stop condition!
	uint16_t counter = 0;
	while(TWCR & (1 << TWSTO)){
		_delay_us(10);
		if(counter++>2000)
			twi_reset();
	}

	// Update TWI state
	twi_state = TWI_READY;
}

void twi_reply(uint8_t ack){
	// Transmit master read ready signal, with or without ack
	if(ack)
		TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWINT) | (1 << TWEA);
	else
		TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWINT);
}

void twi_releaseBus(){
	// Release bus
	TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA) | (1 << TWINT);

	// Update TWI state
	twi_state = TWI_READY;
}

void twi_reset(){
	TWCR = 0;
	_delay_us(10);
	twi_init();
}

ISR(TWI_vect){
	switch(TW_STATUS){
		// All Master
		case TW_START:     // Sent start condition

		case TW_REP_START: // Sent repeated start condition
			// copy device address and r/w bit to output register and ack
			TWDR = twi_slarw;
			twi_reply(1);
			break;

		// Master Transmitter
		case TW_MT_SLA_ACK:  // Slave receiver ACKed address
			
		case TW_MT_DATA_ACK: // Slave receiver ACKed data
			// If there is data to send, send it, otherwise stop
			if(twi_masterBufferIndex < twi_masterBufferLength){
				// Copy data to output register and ack
				TWDR = twi_masterBuffer[twi_masterBufferIndex++];
				twi_reply(1);
			} else {
				if (twi_sendStop){
					twi_stop();
				} else {
					twi_inRepStart = 1;
					TWCR = (1 << TWINT) | (1 << TWSTA)| (1 << TWEN);
					twi_state = TWI_READY;
				}
			}
			break;

		case TW_MT_SLA_NACK:  // Address sent, NACK received
			twi_error = TW_MT_SLA_NACK;
			twi_stop();
			break;

		case TW_MT_DATA_NACK: // Data sent, NACK received
			twi_error = TW_MT_DATA_NACK;
			twi_stop();
			break;

		case TW_MT_ARB_LOST: // Lost bus arbitration
			twi_error = TW_MT_ARB_LOST;
			twi_releaseBus();
			break;

		// Master Receiver
		case TW_MR_DATA_ACK: // Data received, ACK sent
			// Put byte into buffer
			twi_masterBuffer[twi_masterBufferIndex++] = TWDR;
			break;

		case TW_MR_SLA_ACK:  // Address sent, ACK received
			// ACK if more bytes are expected, otherwise NACK
			if(twi_masterBufferIndex < twi_masterBufferLength)
				twi_reply(1);
			else
				twi_reply(0);
			break;

		case TW_MR_DATA_NACK: // Data received, NACK sent
			// Put final byte into buffer
			twi_masterBuffer[twi_masterBufferIndex++] = TWDR;
			if (twi_sendStop){
				twi_stop();
			} else {
				twi_inRepStart = 1;	// we're gonna send the START
				// don't enable the interrupt. We'll generate the start, but we
				// avoid handling the interrupt until we're in the next transaction,
				// at the point where we would normally issue the start.
				TWCR = (1 << TWINT) | (1 << TWSTA)| (1 << TWEN) ;
				twi_state = TWI_READY;
			}
			break;

		case TW_MR_SLA_NACK: // Address sent, NACK received
			twi_stop();
			break;
		// TW_MR_ARB_LOST handled by TW_MT_ARB_LOST case

		// Slave Receiver
		case TW_SR_SLA_ACK:   // addressed, returned ack

		case TW_SR_GCALL_ACK: // addressed generally, returned ack

		case TW_SR_ARB_LOST_SLA_ACK:   // lost arbitration, returned ack

		case TW_SR_ARB_LOST_GCALL_ACK: // lost arbitration, returned ack

			// enter slave receiver mode
			twi_state = TWI_SRX;
			// indicate that rx buffer can be overwritten and ack
			twi_rxBufferIndex = 0;
			twi_reply(1);
			break;

		case TW_SR_DATA_ACK:       // data received, returned ack

		case TW_SR_GCALL_DATA_ACK: // data received generally, returned ack
			// if there is still room in the rx buffer
			if(twi_rxBufferIndex < TWI_BUFFER_LENGTH){
				// put byte in buffer and ack
				twi_rxBuffer[twi_rxBufferIndex++] = TWDR;
				twi_reply(1);
			} else {
				// otherwise nack
				twi_reply(0);
			}
			break;

		case TW_SR_STOP: // stop or repeated start condition received
			// ack future responses and leave slave receiver state
			twi_releaseBus();
			// put a null char after data if there's room
			if(twi_rxBufferIndex < TWI_BUFFER_LENGTH){
				twi_rxBuffer[twi_rxBufferIndex] = '\0';
			}
			// callback to user defined callback
			// since we submit rx buffer to "wire" library, we can reset it
			twi_rxBufferIndex = 0;
			break;

		case TW_SR_DATA_NACK:       // data received, returned nack

		case TW_SR_GCALL_DATA_NACK: // data received generally, returned nack
			// nack back at master
			twi_reply(0);
			break;
		
		// Slave Transmitter
		case TW_ST_SLA_ACK:          // addressed, returned ack

		case TW_ST_ARB_LOST_SLA_ACK: // arbitration lost, returned ack
			// enter slave transmitter mode
			twi_state = TWI_STX;
			// ready the tx buffer index for iteration
			twi_txBufferIndex = 0;
			// set tx buffer length to be zero, to verify if user changes it
			twi_txBufferLength = 0;
			// request for txBuffer to be filled and length to be set
			// note: user must call twi_transmit(bytes, length) to do this
			// if they didn't change buffer & length, initialize it
			if(0 == twi_txBufferLength){
				twi_txBufferLength = 1;
				twi_txBuffer[0] = 0x00;
			}
			// transmit first byte from buffer, fall
			case TW_ST_DATA_ACK: // byte sent, ack returned
			// copy data to output register
			TWDR = twi_txBuffer[twi_txBufferIndex++];
			// if there is more to send, ack, otherwise NACK
			if(twi_txBufferIndex < twi_txBufferLength)
				twi_reply(1);
			else 
				twi_reply(0);
			break;

		case TW_ST_DATA_NACK: // received nack, we are done

		case TW_ST_LAST_DATA: // received ack, but we are done already!
			// ack future responses
			twi_reply(1);
			// leave slave receiver state
			twi_state = TWI_READY;
			break;

		// All
		case TW_NO_INFO:   // No state information
			break;
		case TW_BUS_ERROR: // Bus error, illegal stop/start
			twi_error = TW_BUS_ERROR;
			twi_stop();
			break;
	}
}