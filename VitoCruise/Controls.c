/*
 * Controls.c
 *
 * Created: 26/05/2018 13:24:59
 *  Author: robbe_000
 */ 

 #include "Controls.h"

 #include "CruiseControl.h"

 #define MAX(x, y) (((x) > (y)) ? (x) : (y))
 #define MIN(x, y) (((x) < (y)) ? (x) : (y))

 uint8_t upPressed = 0;
 uint8_t downPressed = 0;
 uint8_t frontPressed = 0;
 uint8_t backPressed = 0;

 void Controls_init(){
	// Set as inputs
	DDRD &= ~((1 << PD2) | (1 << PD3));
	DDRE &= ~((1 << PE4) | (1 << PE5) | (1 << PE6));

	// Disable pull-ups
	PORTD = 0;
	PORTE = 0;
 }

 void Controls_handle(){
	// Check control signal if option enabled
	if(!CHECK_CONTROL || !(PIND & (1 << PD2))){
		// SW_UP
		if(!(PIND & (1 << PD3))){
			if(!upPressed)
				Controls_onUp();

			upPressed++;
			upPressed = MIN(FAST_MODE_LAG, upPressed);

			if(upPressed == FAST_MODE_LAG){
				Controls_onFastUp();
				upPressed -= FAST_MODE_DELAY;
			}
		} else {
			upPressed = 0;
		}

		// SW_DOWN
		if(!(PINE & (1 << PE4))){
			if(!downPressed)
				Controls_onDown();

			downPressed++;
			downPressed = MIN(FAST_MODE_LAG, downPressed);

			if(downPressed == FAST_MODE_LAG){
				Controls_onFastDown();
				downPressed -= FAST_MODE_DELAY;
			}
		} else {
			downPressed = 0;
		}

		// SW_FRONT
		if(!(PINE & (1 << PE5))){
			if(!frontPressed)
				Controls_onFront();

			frontPressed++;
			frontPressed = MIN(FAST_MODE_LAG, frontPressed);

			if(frontPressed == FAST_MODE_LAG){
				Controls_onFastFront();
				frontPressed -= FAST_MODE_DELAY;
			}
		} else {
			frontPressed = 0;
		}
	}

	// SW_BACK (without control)
	if(PINE & (1 << PE6)){
		if(!backPressed)
		Controls_onBack();
		
		backPressed++;
		backPressed = MIN(FAST_MODE_LAG, backPressed);

		if(backPressed == FAST_MODE_LAG){
			Controls_onFastBack();
			backPressed -= FAST_MODE_DELAY;
		}
	} else {
		backPressed = 0;
	}
 }

 void Controls_onUp() {
	CC_incrementSetSpeed();
 }

 void Controls_onDown() {
	CC_decrementSetSpeed();
 }

 void Controls_onFront() {
	CC_switchOn();
 }

 void Controls_onBack() {
	CC_switchReset();
 }

void Controls_onFastUp() {
	CC_incrementSetSpeedFast();
}

void Controls_onFastDown() {
	CC_decrementSetSpeedFast();
}

void Controls_onFastFront() {
}

void Controls_onFastBack() {
}