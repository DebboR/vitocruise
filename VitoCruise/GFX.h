/*
 * GFX.h
 *
 * Created: 9/04/2018 10:16:32
 *  Author: robbe_000
 */ 


#ifndef GFX_H_
#define GFX_H_

#include <math.h>
#include <stdlib.h>

#include "SH1106.h"

#define WIDTH 128
#define HEIGHT 64
#define CHAR_WIDTH 5

void GFX_clearScreen();
void GFX_drawPixel(int16_t x, int16_t y, uint8_t color);
void GFX_drawFastVLine(int16_t x, int16_t y, int16_t h, uint8_t color);
void GFX_drawFastHLine(int16_t x, int16_t y, int16_t w, uint8_t color);
void GFX_fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t color);
void GFX_drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t color);
void GFX_drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t color);
void GFX_drawCircle(int16_t x0, int16_t y0, int16_t r, uint8_t color);
void GFX_drawCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint8_t color);
void GFX_fillCircle(int16_t x0, int16_t y0, int16_t r, uint8_t color);
void GFX_fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint8_t color);
void GFX_drawTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t color);
void GFX_fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t color);
void GFX_drawRoundRect(int16_t x0, int16_t y0, int16_t w, int16_t h, int16_t radius, uint8_t color);
void GFX_fillRoundRect(int16_t x0, int16_t y0, int16_t w, int16_t h, int16_t radius, uint8_t color);
void GFX_drawBitmap(int16_t x, int16_t y, uint8_t *bitmap, int16_t w, int16_t h, uint8_t color, uint8_t bgColor);
void GFX_drawChar(int16_t x, int16_t y, unsigned char c, uint8_t color, uint8_t bgColor, uint8_t size);
void GFX_drawString(int16_t x, int16_t y, unsigned char *str, uint8_t length, uint8_t color, uint8_t bgColor, uint8_t size);
void GFX_drawVProgressBar(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t range, uint16_t value, uint8_t color);
void GFX_drawHProgressBar(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t range, uint16_t value, uint8_t color);

#endif /* GFX_H_ */