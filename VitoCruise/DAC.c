/*
 * DAC.c
 *
 * Created: 29/04/2018 18:04:49
 *  Author: robbe_000
 */ 

#include "DAC.h"

void DAC_init(){
	// SPI Interrupt disabled
	// SPI enable
	// Data order: MSB first
	// Master select
	// Clock low when idle
	// Data on rising edge
	// Clock divider: 16
	SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0);

	// Set outputs
	DDRB |= (1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB4);
	PORTB |= (1 << PB0);
}

void DAC_sendCommand(uint16_t command){
	// Pull CS low
	PORTB &= ~(1 << PB4);
	
	// Send MSB
	SPDR = (command >> 8);

	// Wait until transmitted
	while (!(SPSR & (1 << SPIF)));

	// Send LSB
	SPDR = (command & 0xFF);

	// Wait until transmitted
	while (!(SPSR & (1 << SPIF)));

	// Pull CS high
	PORTB |= (1 << PB4);
}

void DAC_setDAC0(uint16_t value){
	uint16_t command = (1 << 12) | (value & 0x0FFF);
	DAC_sendCommand(command);
}

void DAC_setDAC1(uint16_t value){
	uint16_t command = (1 << 15) | (1 << 12) | (value & 0x0FFF);
	DAC_sendCommand(command);
}