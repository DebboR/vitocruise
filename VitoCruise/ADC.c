/*
 * ADC.c
 *
 * Created: 27/04/2018 23:57:04
 *  Author: robbe_000
 */ 

#include <avr/io.h>

#include "ADC.h"

uint16_t ADC0Result = 0;
uint16_t ADC1Result = 0;
uint8_t sampling1 = 0;

void ADC_init(){
	// ADC Reference: AREF
	// Initial multiplexer: ADC0
	// Right adjust in H and L registers
	// ADC enabled
	// Start first conversion
	// No auto-trigger
	// Enable interrupts
	// Prescaler: 1/64
	// Disable I/O on ADC0 and ADC1

	ADCSRA |= (1 << ADEN) | (1 << ADSC) | (1 << ADIE) | (1 << ADPS2) | (1 << ADPS1);
	DIDR0 |= (1 << ADC1D) | (1 << ADC0D);
}

uint16_t ADC_getADC0(){
	return ADC0Result;
}

uint16_t ADC_getADC1(){
	return ADC1Result;
}

// ADC interrupt on conversion finished
ISR(ADC_vect){
	// Save data
	if(sampling1){
		ADC1Result = ADCL | (ADCH << 8);
		ADMUX &= 0b11100000;
		sampling1 = 0;
	} else {
		ADC0Result = ADCL | (ADCH << 8);
		ADMUX &= 0b11100000;
		ADMUX |= (1 << MUX0);
		sampling1 = 1;
	}

	// Start new conversion
	ADCSRA |= (1 << ADSC);
}