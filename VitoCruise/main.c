/*
 * VitoCruise.c
 *
 * Created: 11/25/2017 3:13:52 AM
 * Author : robbe_000
 */ 

#ifndef  F_CPU
	#define F_CPU 8000000UL
#endif

//#define DETAIL_VIEW

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include "GFX.h"
#include "twiLib.h"
#include "ADC.h"
#include "DAC.h"
#include "Throttle.h"
#include "Controls.h"
#include "MainView.h"
#include "DetailView.h"
#include "Watchdog.h"
#include "Security.h"

void TIMER_init(){
	// Timer 0
	// Mode: Normal
	// Clock divider: 1024 (30.5 Hz)
	// Enable overflow interrupt
	TCCR0A = (1 << CS02) | (1 << CS00);
	TIMSK0 = (1 << TOIE0);
}

void init(){
	// Enable global interrupts
	sei();

	// Init all modules
	SH1106_init();
	ADC_init();
	DAC_init();
	TIMER_init();
	Speed_init();
	Controls_init();
	Watchdog_init();
	Security_init();
}

int main(){
	init();
    while (1) {
		// Handle input
		Controls_handle();

		// Write rendered buffer to screen
		SH1106_display();

		// Render
		GFX_clearScreen();

		#ifdef DETAIL_VIEW
			DetailView_render();
		#else
			MainView_render();
		#endif

		// Kick watchdog
		Watchdog_kick(WATCHDOG_MODULE_MAIN_LOOP);
    }
	return 0;
}

// Timer overflow
// Time-critical operations here
ISR(TIMER0_OVF_vect){
	// Calculate CC
	CC_calculate();

	// Set output
	uint16_t throttlePosition = Throttle_getPosition();
	uint16_t ccPosition = (uint16_t) CC_getIntegerResult();

	if(throttlePosition > ccPosition){
		Throttle_setPosition(throttlePosition);
	} else {
		Throttle_setPosition(ccPosition);
	}
	
	// Check security
	Security_check();

	// Kick watchdog
	Watchdog_kick(WATCHDOG_MODULE_CRITICAL_LOOP);
}
