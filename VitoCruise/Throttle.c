/*
 * Throttle.c
 *
 * Created: 28/04/2018 0:49:34
 *  Author: robbe_000
 */ 

#include "Throttle.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

uint16_t ThrottlePosition = 0;

uint16_t Throttle_getPosition(){
	uint16_t adc0 = ADC_getADC0();
	uint16_t adc1 = 2*ADC_getADC1();
	int16_t diff = adc0 - adc1;

	if(abs(diff) > MAX_CHANNEL_DIFFERENCE){
		//Todo: Handle exception!
		return 0;
	}

	return adc0;
}

uint16_t Throttle_getPositionScaled(uint16_t minVal, uint16_t maxVal){
	return MIN(maxVal, MAX(minVal, (uint16_t) round(((float) (Throttle_getPosition() - THROTTLE_MIN_POS)) * ((float) (maxVal - minVal) / (THROTTLE_MAX_POS - THROTTLE_MIN_POS)) + minVal)));
}

void Throttle_setPosition(uint16_t position){
	// Set borders
	position = MIN(THROTTLE_SET_MAX_POS, MAX(THROTTLE_SET_MIN_POS, position));

	// Save current value
	ThrottlePosition = position;

	// Convert from 10bit position (0 - 4.5V) to 12bit position (0-4V)
	uint16_t scaledPosition = (position << 2);

	DAC_setDAC0(scaledPosition);
	DAC_setDAC1(scaledPosition >> 1);
}

uint16_t Throttle_getSetPosition(){
	return ThrottlePosition;
}

uint16_t Throttle_getSetPositionScaled(uint16_t minVal, uint16_t maxVal){
	return MIN(maxVal, MAX(minVal, (uint16_t) round(((float) (Throttle_getSetPosition() - THROTTLE_SET_MIN_POS)) * ((float) (maxVal - minVal) / (THROTTLE_SET_MAX_POS - THROTTLE_SET_MIN_POS)) + minVal)));
}

uint16_t Throttle_limit(float input){
	return (uint16_t) MIN(THROTTLE_SET_MAX_POS, MAX(THROTTLE_SET_MIN_POS, input));
}