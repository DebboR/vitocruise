/*
 * Security.c
 *
 * Created: 29/05/2018 23:27:37
 *  Author: robbe_000
 */ 


 #include "Security.h"
 #include "Watchdog.h"
 #include "CruiseControl.h"


void Security_init(){
	// Set brake switch as input
	DDRB &= ~(1 << PB3);
}

uint8_t Security_isBraking(){
	return (PINB & (1 << PB3));
}

void Security_check(){
	// Check switch
	if(Security_isBraking())
		CC_switchOff();

	// Check minimum and maximum speed
	uint8_t speed = Speed_getIntegerSpeed();
	if(speed < MIN_SPEED || speed > MAX_SPEED)
		CC_switchOff();

	// TODO: Implement checking of not being able to reach speed
	
	// Kick watchdog	
	Watchdog_kick(WATCHDOG_MODULE_SECURITY);
}